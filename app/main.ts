import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import {RedditAppModule} from './my-app/module';

platformBrowserDynamic().bootstrapModule(RedditAppModule);