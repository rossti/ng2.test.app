import { Component } from '@angular/core';

@Component ({
    selector: 'my-app',
    styleUrls: ['./app/my-app/my-app.css'],
    template: `
        <form class="ui large form segment">
            <h3 class="ui header">Добавить еще одного пользователя</h3>
            <div class="field">
                <input type="text" name="name" placeholder="имя пользователя" maxlength="10" #newName>
            </div>
            <div class="field">
                <input type="text" name="number" maxlength="8" placeholder="оценка пользователя" #newNumber>
            </div>
            <button class="ui positive right floated button" (click)="addArticle(newName, newNumber)">Отправить</button>
        </form>
        <h2>Список пользователей</h2>
        <ul>
            <li *ngFor="let user of Users;  let i=index">
                <div class="user-name">{{user.name}}</div> 
                <div class="user-number">{{user.id}}</div>
                <div class="user-remove" (click)="deleteUser(i)">remove</div>
            </li>
        </ul>
              `

})
export class MyAppComponent{
    Users = USERS;

    addArticle(newName: HTMLInputElement, newNumber: HTMLInputElement) {
        var curUser = newName.value.substr(0, newName.value.length-1) + newName.value.substr(newName.value.length-1, newName.value.length).toUpperCase();
        var curNumb = newNumber.value;
        USERS.push({id: curNumb, name: curUser});
        newName.value = '';
        newNumber.value = '';
        return false;
    };
    deleteUser($index) {
        USERS.splice($index, 1);
        return false;
    }

}
var USERS = [
    {"id": "3", "name": "Bobby"},
    {"id": "5", "name": "John"},
    {"id": "8", "name": "William"},
    {"id": "9", "name": "Bruce"},
    {"id": "2", "name": "Elizabeth"}
];
