# Тестовое задание по Angular 2

> Тестовое задние Василинки Ростислава по созданию списка пользователей на Angular2

## Зависимости

Прежде чем начать, вы должны убедиться что у вас установлен node.js. Так как мы будем использовать `npm` для установки всех зависимостей. Вы можете установить node.js по [ссылке здесь](https://nodejs.org/en/).

## Установка

Во-первых, убедитесь что вы установили все зависимости, для этого в вашей Terminal выполните команду:

```
npm install
```

После этого

- мы будем компилировать наш TypeScript код
- и запустим локальный сервер с нашим приложением

для этого выполните команду:

```
npm run go
```

После этого перейдите по ссылке [http://localhost:3000](http://localhost:3000) в вашем браузере.